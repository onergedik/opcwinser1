using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Mail;
using System.Threading;
using System.Threading.Tasks;
using Dapper;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using OPCAutomation;

namespace OPCWinServisi
{
    public class Worker : BackgroundService, IDisposable
    {
        private Timer _timer5sn;
        private readonly ILogger<Worker> _logger;
        public OPCServer OPCSrv = new OPCServer();
        public OPCGroup opcgrp;
        public OPCGroup opcgrp2;
        public OPCGroups opcgrps;
        public OPCItem[] DataF1 = new OPCItem[1000];
        object ItemValues;
        object Qualities;
        object TimeStamps;
        public OPCGroup opcgrpCyle;
        public OPCItem[] DataCyle = new OPCItem[1000];
        public String[] StrEskiDeger = new String[1000];
        public int intDeviceNo;
        public int intOpcCount;
        public int intOpcCountCycle;
        object ItemValues2;//true false gelene rakam rakama true false gelmi�ti objesine g�cenmedi�im i�in ay�lm��
        object Qualities2;
        object TimeStamps2;
        private string _delimiter = ".";
        private string _strKepwareVersiyon = "KEPWare.KepServerEX.V6";
        public string _defaultGroupName = "KAYAHAN";
        public string strDeviceNo = "";
        public int strAlarm_Durumu = 0; public int strCycle_Start = 0; public int strSpindle_Load = 0; public int strCutting = 0; public int strBad = 0;
        //        Array OPCItemIDs = Array.CreateInstance(typeof(string), 10);
        int StrValue = 0; string StrItem = ""; string StrDate = "";
       // private readonly mailsettings _mailsettings;

        public Worker(ILogger<Worker> logger)
        {
            _logger = logger;

        }
        public override Task StopAsync(CancellationToken cancellationToken)
        {
            _logger.LogInformation("servis kapat�ld�");
           // _timer1dk?.Change(Timeout.Infinite, 0);
            _timer5sn?.Change(Timeout.Infinite, 0);
            return Task.CompletedTask;
           // return base.StopAsync(cancellationToken);
        }

    

    private async void DoWork5sn(object _)
    {
            try
            {
                intDeviceNo = 0;
                string StrValue = ""; string StrItem = ""; 
                for (int i = 0; i < intOpcCountCycle; i++)
                {
                    DataCyle[i + 1].Read(1, out ItemValues2, out Qualities2, out TimeStamps2);
                    if (Convert.ToDecimal(Qualities2) == 192)
                    {
                        StrValue = Convert.ToString(ItemValues2);
                        StrItem = DataCyle[i + 1].ItemID.ToString();
                        if (StrEskiDeger[i + 1] != StrValue)//de�er de�i�mi� ise 
                        {
                            vOkuYazData(StrItem, intDeviceNo);
                            StrEskiDeger[i + 1] = StrValue;
                        }
                    }
                }
//                _logger.LogInformation("DoWork5sn");
            }
            catch (System.Exception err)
            {
                _logger.LogError(err.Message.ToString());
            }
     }
    public override Task StartAsync(CancellationToken cancellationToken)
        {
            while (BaglanOk() == false)
            {
                Thread.Sleep(1000);
            }
          //  _timer1dk = new Timer(DoAnotherWork1dk, null, TimeSpan.FromSeconds(60), TimeSpan.FromSeconds(60));
            _timer5sn = new Timer(DoWork5sn, null, TimeSpan.FromSeconds(5), TimeSpan.FromSeconds(5));
            //TimeSpan.Zero
            sendEmail("oner.gedik@tezmaksan.com;ozercem.kelahmet@tezmaksan.com","opc Serivis �al��t�","Ba�land�.");
            _logger.LogInformation("servis �al��t�");
            //base.StartAsync(cancellationToken);//taskExecuteAsync  
            return Task.CompletedTask;
           // return base.StartAsync(cancellationToken);
        }
        public class mailsettings
        {
            public string smtpserver { get; set; }
        }
        public static string[] ListOPCServers(string nodeName = "127.0.0.1")
        {
            List<string> list = new List<string>();
            if (!String.IsNullOrEmpty(nodeName))
            {
                try
                {
                    OPCServer server = new OPCServer();
                    object result = server.GetOPCServers(nodeName);
                    if (result != null && result is Array)
                    {
                        Array servers = result as Array;
                        foreach (string s in servers)
                        {
                            list.Add(s);
                        }
                    }
                }
                catch (Exception )
                {
                }
            }
            return list.ToArray();
        }
        public bool BaglanOk()
        {
            try
            {
                string strIp = "127.0.0.1";// "127.0.0.1";192.68.1.58
                if (!String.IsNullOrEmpty(strIp))
                {
                    string[] serverList = ListOPCServers(strIp);
                    if (serverList.Length != 0)
                    {
                        foreach (string turn in (Array)serverList)
                        {
                            _strKepwareVersiyon = turn;
                        }
                    }
                    else
                    {
                        return false;
                    }
                }
                OPCSrv.Connect(_strKepwareVersiyon);
                opcgrps = OPCSrv.OPCGroups;
                opcgrp = opcgrps.Add("Tezmaksan");
                opcgrpCyle = opcgrps.Add("TezmaksanCycle");// cycle_start = 1 yada 0 oldumu 

                OPCItems opcitms = opcgrp.OPCItems;
                OPCItems opcitmsCyle = opcgrpCyle.OPCItems;
                //pcgrpCyle.DataChange += new DIOPCGroupEvent_DataChangeEventHandler(ObjOPCGroup_DataChange);
                opcgrp.UpdateRate = 1000;
                opcgrp.IsActive = true;
                opcgrp.IsSubscribed = true;

                opcgrpCyle.UpdateRate = 1000;
                opcgrpCyle.IsActive = true;
                opcgrpCyle.IsSubscribed = true;

                OPCBrowser oPCBrowser = OPCSrv.CreateBrowser();
                oPCBrowser.ShowBranches();
                oPCBrowser.ShowLeafs(true);
                int i = 1;
                int c = 1;

                var myList5 = new List<string>() { "alarm_durumu", "cycle_start", "_system._error", "spindle_load", "cuttting" };
                var myList3ludegisen = new List<string>() { "alarm_durumu", "cycle_start", "_system._error" };

                foreach (object turn in oPCBrowser)
                {
                    if (turn.ToString().ToLower().Contains("kayahan "))
                    {
                        foreach (string item in myList5)//Iterate through each item.
                        {
                            if (turn.ToString().ToLower().Contains(item))
                            {
                                DataF1[i] = opcitms.AddItem(turn.ToString(), i);
                                i++;
                            }
                        }
                        foreach (string item in myList3ludegisen)//Iterate through each item.
                        {
                            if (turn.ToString().ToLower().Contains(item))
                            {
                                DataCyle[c] = opcitmsCyle.AddItem(turn.ToString(), c);
                                c++;
                            }
                        }
                    }
                }

                intOpcCount = opcitms.Count;
                intOpcCountCycle = opcitmsCyle.Count;
                return true;
            }
            catch (Exception err)
            {
                if (err.Message.GetHashCode() == 730091270)//
                {
                  _logger.LogInformation("hatal� Tag "+ err.Message);
                }
                this.OPC_Baglanti_Kes();
                return false;
            }

        }
        public void OPC_Baglanti_Kes()
        {
            if (OPCSrv.ServerState == 1)
            {
                OPCSrv.OPCGroups.RemoveAll();
                OPCSrv.Disconnect();
            }
            _logger.LogInformation("ba�lant� kesildi");
        }
      
        private void vOkuYazData(string intDeviceId, int intDeviceIdOld)
        {
            try
            {
                string strM = sMachineId(intDeviceId);
                intDeviceNo = (int)Enum.Parse(typeof(eDeviceType), strM.Replace(".", ""));
                if (intDeviceNo == intDeviceIdOld) return;
                strAlarm_Durumu = 0; strCycle_Start = 0; strBad = 0; strSpindle_Load = 0; strCutting = 0;

                for (int i = 0; i < intOpcCount; i++)
                {
                    if (DataF1[i + 1].ItemID.Contains(strM))
                    {
                        //OPC data okunuyor 
                        DataF1[i + 1].Read(1, out ItemValues, out Qualities, out TimeStamps);
                        if (Convert.ToDecimal(Qualities) == 192)
                        {
                            //DataF1[i + 1].Write("1"); istersek yazabiliriz de
                            StrItem = DataF1[i + 1].ItemID.ToString();
                            StrValue = Convert.ToInt32(ItemValues);
                            StrDate = DataF1[i + 1].TimeStamp.ToString();

                            if (StrItem.ToLower().Contains("alarm"))
                            { strAlarm_Durumu = StrValue; }
                            else if (StrItem.ToLower().Contains("cycle"))
                            { strCycle_Start = Convert.ToInt32(StrValue); }
                            else if (StrItem.ToLower().Contains("error"))
                            { strBad = Convert.ToInt32(StrValue); }
                            else if (StrItem.ToLower().Contains("spindle"))
                            { strSpindle_Load = Convert.ToInt32(StrValue); }
                            else if (StrItem.ToLower().Contains("cuttting"))
                            { strCutting = Convert.ToInt32(StrValue); }
                        }
                    }
                }
                InsertDappersToSQL();
            }
            catch (System.Exception err)
            {
                _logger.LogError(err, "vOkuYazData");
            }
        }
        public void InsertDappersToSQL()
        {
            string sql = "SP_Insert_tmp";
            var dictionary = new Dictionary<string, object>
                {
                    { "@Param", 1},
                    { "@DeviceId", intDeviceNo.ToString()},
                    { "@Alarm_Durumu",  strAlarm_Durumu},
                    { "@Cycle_Start",  strCycle_Start},
                    { "@Bad",  strBad},
                    { "@Spindle_Load",  strSpindle_Load},
                    { "@Cutting", strCutting}
                };
            var parameters = new DynamicParameters(dictionary);

            using (var connection = ConnectionFactory.GetOpenConnection())
            {
                var affectedRows = connection.Execute(sql,
                parameters,
                commandType: CommandType.StoredProcedure);
            }
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
             //   vOkuYaz();
                _logger.LogInformation("�al��ma zaman�: {time}", DateTimeOffset.Now);
                // sendEmail("oner.gedik@tezmaksan.com", "konu", "mail a��klama ");
                await Task.Delay(5000, stoppingToken);//5sn
            }
        }

        public static void sendEmail(string toEmail,string subject="", string body="")
        {
            try
            {
                var Client = new SmtpClient("smtp.office365.com", 587);
                MailMessage message = new MailMessage("bilgilendirme@tezmaksan.com", toEmail);
                message.Body = body;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                message.Subject = subject;
                message.SubjectEncoding = System.Text.Encoding.UTF8;
                Client.EnableSsl = true;
                Client.Credentials = new System.Net.NetworkCredential("bilgilendirme@tezmaksan.com", "Gdm2796*");
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls;
                Client.EnableSsl = true;
                Client.Send(message);
                message.Dispose();
            }
            catch (Exception)
            {
                throw;
            }
        }
        private void ObjOPCGroup_DataChange(int TransactionID, int NumItems, ref Array ClientHandles, ref Array ItemValues, ref Array Qualities, ref Array TimeStamps)
        {
            //for (int i = 1; i <= NumItems; i++)
            //{
            //    if ((Convert.ToInt32(ClientHandles.GetValue(i)) == 1))
            //    {
            //        _logger.LogInformation("change den gelen value: {time}", ItemValues.GetValue(i).ToString()); 
            //    }
            //}
        }
        public enum eDeviceType
        {
            A2IM04 = 21,
            A2IM05 = 22,
            A2IM10 = 23,
            A2IM11 = 24,
            A2IM12 = 25,
            A2TR17 = 26,
            A2TR18 = 27,
            A2TR19 = 28,
            A2TR2 = 29,
            A2TR20 = 30,
            A2TR21 = 31,
            A2TR22 = 32,
            A2TR5 = 33,
            A2IM01 = 34,
            A2IM02 = 35,
            A2IM03 = 36,
            A2IM08 = 37,
            A2IM09 = 38,
            A2IM13 = 39,
            A4IM01 = 40,
            A4IM02 = 41,
            A5lM01 = 42,
            A2TR01 = 43,
            A2TR03 = 44,
            A2TR04 = 45,
            A2TR06 = 46,
            A2TR07 = 47,
            A2TR08 = 48,
            A2TR09 = 49,
            A2TR10 = 50,
            A2TR13 = 51,
            A2TR14 = 52,
            A2TR15 = 53,
            A2TR16 = 54,
            A4TR01 = 55,
            A4TR02 = 56
        }
        private string sMachineId(string tagName)
        {
            int i = tagName.IndexOf(System.Convert.ToChar(Delimiter));
            if (i == -1) return "";
            return tagName.Substring(0, i + 1).Replace(_defaultGroupName, "").Trim();
        }
        public string Delimiter
        {
            get { return _delimiter; }
            set { _delimiter = value; }
        }
    }
}
