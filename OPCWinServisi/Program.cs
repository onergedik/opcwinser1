using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using Serilog.Events;


namespace OPCWinServisi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .MinimumLevel.Override("Microsoft", LogEventLevel.Warning)
                .Enrich.FromLogContext()
                .WriteTo.File(@"c:\temp\OPCWinServisi\logFile.txt")
                .CreateLogger();
            try
            {
                Log.Information("Servis start oldu.");
                CreateHostBuilder(args).Build().Run();
                return;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "start serviste hata var");
                return;
                //throw;
            }
            finally
            {
                Log.CloseAndFlush();
            }
          
        }
        public class mailsettings
        {
            public string smtpserver { get; set; }
        }
        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
            .UseWindowsService()
                .ConfigureServices((hostContext, services) =>
                {
                    services.
                    AddOptions()
                    .Configure<mailsettings>(hostContext.Configuration.GetSection("mailsettings"))

                    .AddHostedService<Worker>();
                })
            .UseSerilog();
    }
}
